class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @product_properties = @product.product_properties.includes(:property)
    @related_products = Spree::Product.
      includes(master: [:images, :default_price]).
      in_taxons(@product.taxons).
      without_products(@product.id).
      distinct.
      order("RAND()").
      limit(4)
  end
end
