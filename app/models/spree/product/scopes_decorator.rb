Spree::Product.class_eval do
  add_search_scope :without_products do |*ids|
    where.not(id: ids)
  end
end
