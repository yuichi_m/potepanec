require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    # taxonomy, taxon
    let!(:category) { create(:taxonomy, name: 'Category') }
    let!(:bag) { create(:taxon, parent: category.root, name: 'Bag', taxonomy: category) }
    let!(:mug) { create(:taxon, parent: category.root, name: 'Mug', taxonomy: category) }

    # product
    let!(:tiger_mug) { create(:product, name: 'Tiger Mug', taxons: [mug]) }
    let!(:panda_bag) { create(:product, name: 'Panda Bag', taxons: [bag]) }
    let!(:lion_bag) { create(:product, name: 'Lion Bag', taxons: [bag]) }
    let!(:bear_bag) { create(:product, name: 'Bear Bag', taxons: [bag]) }
    let!(:dog_bag) { create(:product, name: 'Dog Bag', taxons: [bag]) }
    let!(:pig_bag) { create(:product, name: 'Pig Bag', taxons: [bag]) }
    let!(:fox_bag) { create(:product, name: 'Fox Bag', taxons: [bag]) }

    # property
    let!(:brand) { create(:property, name: 'brand', presentation: 'Brand') }
    let!(:country) { create(:property, name: 'country', presentation: 'Country') }
    let!(:pb) { create(:product_property, product: panda_bag, property: brand, value: 'PB') }
    let!(:jp) { create(:product_property, product: panda_bag, property: country, value: 'JP') }

    before { get :show, params: { id: panda_bag.id } }

    it "responds successfully" do
      expect(response).to be_success
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end

    it "assigns the requested product to @product" do
      expect(assigns(:product)).to eq panda_bag
    end

    it "assigns the properties of the requested product to @product_properties" do
      expect(assigns(:product_properties)).to contain_exactly(pb, jp)
    end

    it "assigns the products related to the requested product to @related_products" do
      expect(assigns(:related_products).length).to eq 4 # 関連商品は4つだけ表示する
      expect(assigns(:related_products)).not_to include tiger_mug # 別カテゴリは表示しない
    end

    it "renders show template" do
      expect(response).to render_template :show
    end
  end
end
