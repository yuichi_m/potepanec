require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  given!(:category) { create(:taxonomy, name: 'Category') }
  given!(:bag) { create(:taxon, parent: category.root, name: 'Bag', taxonomy: category) }
  given!(:mug) { create(:taxon, parent: category.root, name: 'Mug', taxonomy: category) }
  given!(:panda_bag) do
    create(:product, name: 'Panda Bag', price: 19.99, description: 'Cute Bag!!', taxons: [bag])
  end
  given!(:lion_bag) { create(:product, name: 'Lion Bag', price: 29.99, taxons: [bag]) }
  given!(:tiger_mug) { create(:product, name: 'Tiger Mug', price: 39.99, taxons: [mug]) }
  given!(:rabbit_mug) { create(:product, name: 'Rabbit Mug', price: 49.99, taxons: [mug]) }

  background { visit potepan_category_path(bag.id) }

  scenario 'page title' do
    expect(page).to have_title 'Bag | BIGBAG Store'
  end

  scenario 'page header' do
    within '.pageHeader' do
      expect(page).to have_content 'Home'
      expect(page).to have_content 'Shop'
      expect(page).to have_content('Bag', count: 2)
    end
  end

  scenario 'sidebar' do
    within '.sideBar' do
      expect(page).to have_content 'Category'
      expect(page).to have_content 'Bag (2)'
      expect(page).to have_content 'Mug (2)'
    end
  end

  scenario 'products list' do
    within '.productGrid' do
      expect(page).to have_content 'Panda Bag'
      expect(page).to have_content '19.99'
      expect(page).to have_content 'Lion Bag'
      expect(page).to have_content '29.99'
      expect(page).not_to have_content 'Tiger Mug'
      expect(page).not_to have_content '39.99'
      expect(page).not_to have_content 'Rabbit Mug'
      expect(page).not_to have_content '49.99'
    end
  end

  scenario 'move to product show page' do
    click_link 'Panda Bag'
    expect(page).to have_current_path potepan_product_path(panda_bag.id)
  end
end
