require 'rails_helper'

RSpec.feature "Products", type: :feature do
  feature '#show detailed product page' do
    # taxonomy, taxon
    given!(:category) { create(:taxonomy, name: 'Category') }
    given!(:bag) { create(:taxon, parent: category.root, name: 'Bag', taxonomy: category) }
    given!(:mug) { create(:taxon, parent: category.root, name: 'Mug', taxonomy: category) }

    # product
    given!(:panda_bag) do
      create(:product, name: 'Panda Bag', description: 'cute bag!', price: 19.99, taxons: [bag])
    end
    given!(:lion_bag) { create(:product, name: 'Lion Bag', price: 29.99, taxons: [bag]) }
    given!(:bear_bag) { create(:product, name: 'Bear Bag', price: 39.99, taxons: [bag]) }
    given!(:tiger_mug) { create(:product, name: 'Tiger Mug', price: 49.99, taxons: [mug]) }

    # property
    given!(:brand) { create(:property, name: 'brand', presentation: 'Brand') }
    given!(:country) { create(:property, name: 'country', presentation: 'Country') }
    given!(:pb) { create(:product_property, product: panda_bag, property: brand, value: 'PB') }
    given!(:jp) { create(:product_property, product: panda_bag, property: country, value: 'JP') }

    # variant
    given!(:size) { create(:option_type, name: 'size', presentation: 'Size') }
    given!(:size_s) { create(:option_value, name: 's', presentation: 'S', option_type: size) }
    given!(:size_m) { create(:option_value, name: 'm', presentation: 'M', option_type: size) }
    given!(:variant_size_s) { create(:variant, product: panda_bag, option_values: [size_s]) }
    given!(:variant_size_m) { create(:variant, product: panda_bag, option_values: [size_m]) }

    background { visit potepan_product_path(panda_bag.id) }

    scenario 'show product basic information' do
      within '#cart-form' do
        # product
        expect(page).to have_content 'Panda Bag'
        expect(page).to have_content 'cute bag!'
        expect(page).to have_content '19.99'

        # variant
        expect(page).to have_select 'guiest_id3', options: ['Size: S', 'Size: M']

        # property
        expect(page).to have_content 'Brand'
        expect(page).to have_content 'PB'
        expect(page).to have_content 'Country'
        expect(page).to have_content 'JP'
      end
    end

    scenario 'show related products' do
      within '.productsContent' do
        expect(page).to have_content 'Lion Bag'
        expect(page).to have_content '29.99'
        expect(page).to have_content 'Bear Bag'
        expect(page).to have_content '39.99'
        # 別カテゴリーは表示されない
        expect(page).not_to have_content 'Tiger Mug'
        expect(page).not_to have_content '49.99'
      end
    end

    scenario 'move to cart page' do
      click_link 'カートへ入れる'
      expect(page).to have_current_path potepan_cart_page_path
    end
  end
end
