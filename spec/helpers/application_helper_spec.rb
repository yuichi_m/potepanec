require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe "#full_title" do
    context "page title is specified" do
      it "returns full title" do
        expect(full_title(page_title: "Sample Page")).to eq "Sample Page | BIGBAG Store"
      end
    end

    context "page title is not specified" do
      it "returns base title" do
        expect(full_title).to eq "BIGBAG Store"
      end
    end
  end
end
