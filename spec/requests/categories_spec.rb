require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "#show" do
    let!(:category) { create(:taxonomy, name: 'Category') }
    let!(:brand) { create(:taxonomy, name: 'Brand') }

    let!(:bag) { create(:taxon, parent: category.root, name: 'Bag', taxonomy: category) }
    let!(:mug) { create(:taxon, parent: category.root, name: 'Mug', taxonomy: category) }

    let!(:panda_bag) { create(:product, name: 'Panda Bag', taxons: [bag]) }
    let!(:lion_bag) { create(:product, name: 'Panda Bag', taxons: [bag]) }
    let!(:tiger_mug) { create(:product, name: 'Tiger Mug', taxons: [mug]) }

    it "responds successfully" do
      get potepan_category_path(bag.id)
      expect(response).to be_success
      expect(response).to have_http_status "200"
    end

    it "renders show template" do
      get potepan_category_path(bag.id)
      expect(response).to render_template(:show)
    end

    it "assigns all taxonomy to @taxonomies" do
      get potepan_category_path(bag.id)
      expect(assigns(:taxonomies)).to contain_exactly(category, brand)
    end

    it "assigns the requested taxon to @taxon" do
      get potepan_category_path(bag.id)
      expect(assigns(:taxon)).to eq bag
    end

    it "assigns the requested products to @products" do
      get potepan_category_path(bag.id)
      expect(assigns(:products)).to contain_exactly(panda_bag, lion_bag)
    end

    it "assigns false to @display_in_list_format when display parameter is not 'list'" do
      get potepan_category_path(bag.id)
      expect(assigns(:display_in_list_format)).to be_falsey
    end

    it "assigns true to @display_in_list_format when display parameter is 'list'" do
      get potepan_category_path(bag.id, display: 'list')
      expect(assigns(:display_in_list_format)).to be_truthy
    end
  end
end
